# Stocks

## node version

- please use v14.17.4

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

### Project Setup

- The implementation uses the following:

  - VueJS (v3)
  - Vue-Router (v4)
  - Vuex (v4)
  - TailwindCSS

- Approach:

  - Following state are centralized and maintained @ ./src/store/stocks.js

    - all : current stock/company recevived via the API call
    - filters : filter payload for API call
    - pagination : used for showing more items + API call
    - app : maintains state of the application

  - Application starts @ views/Home.vue (via router).
  - On created event fetch/stocks action gets dispached is dispatched.
  - All mutation and actions are maintained @ ./src/store/stocks.js
  - All Unit tests for Home.vue can be found @ ./tess/home.specs.js
