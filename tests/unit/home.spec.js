import { mount, shallowMount } from "@vue/test-utils";
import Home from "@/views/Home";
import Store from "@/store";
import { createStore } from "vuex";

describe("Home.vue", () => {
  it("renders correct title when component has been created created", () => {
    const wrapper = shallowMount(Home, {
      global: {
        mocks: {
          $store: Store,
        },
      },
    });
    expect(wrapper.find('[data-test="title"]').text()).toMatch("Global Stocks");
  });

  it("renders stock cards when data available", async () => {
    const store = createStore({
      state() {
        return {
          stocks: {
            app: {
              market: "Global",
              isLoading: false,
              hasError: false,
            },
            all: [
              {
                name: "Canyon Silver Mines",
                ticker_symbol: "CANY",
                score: {
                  data: {
                    value: 0,
                    income: 0,
                    health: 0,
                    past: 0,
                    future: 0,
                    management: 0,
                    misc: 0,
                    total: 0,
                    sentence: "Weak fundamentals or lack of information.",
                  },
                },
              },
              {
                name: "MediForce",
                ticker_symbol: "MDFC",
                score: {
                  data: {
                    value: 0,
                    income: 0,
                    health: 0,
                    past: 0,
                    future: 0,
                    management: 0,
                    misc: 0,
                    total: 0,
                    sentence: "Weak fundamentals or lack of information.",
                  },
                },
              },
            ],
            pagination: {
              currentPage: 1,
              itemsPerPage: 2,
              totalItems: 4,
              isLoading: false,
            },
          },
        };
      },
      actions: {
        "stocks/fetchStocks": () => {},
      },
    });

    const wrapper = mount(Home, {
      global: {
        stubs: {
          Chart: {
            template: "<span />",
          },
        },
        mocks: {
          $store: store,
        },
      },
    });
    await wrapper;

    // card exists
    expect(wrapper.find('[data-test="stock-card"]').exists()).toBe(true);

    // look for
    expect(wrapper.text()).toMatch("CANY");
    expect(wrapper.text()).toMatch("MDFC");
  });

  // select onchange updates current market text
  it("updates title when selecting country select item", async () => {
    const selectValue = {
      value: "au",
      label: "Australia",
    };
    const wrapper = mount(Home, {
      global: {
        stubs: {
          Chart: {
            template: "<span />",
          },
        },
        mocks: {
          $store: Store,
        },
      },
    });
    await wrapper
      .find("select[name=country]")
      .setValue(JSON.stringify(selectValue));

    expect(wrapper.find('[data-test="title"]').text()).toMatch(
      `${selectValue.label} Stocks`
    );
  });
});
