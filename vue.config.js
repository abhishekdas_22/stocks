module.exports = {
  configureWebpack: {
    devtool: "source-map",
  },
  pages: {
    index: {
      entry: "src/main.js",
      title: "Stocks",
    },
  },
};
