import axios from "axios";
export const stocks = {
  namespaced: true,

  state() {
    return {
      all: [],
      filters: {
        countryFilter: [],
        sortByFilter: ["order_by", "market_cap", "desc"],
        defaultFilters: [
          ["primary_flag", "=", true],
          ["grid_visible_flag", "=", true],
          ["market_cap", "is_not_null"],
          ["is_fund", "=", false],
        ],
      },
      pagination: {
        currentPage: 0,
        itemsPerPage: 8,
        totalItems: 0,
        isLoading: false,
      },
      app: {
        market: "Global",
        isLoading: false,
        hasError: false,
      },
    };
  },

  mutations: {
    UPDATE_STOCKS(state, stocks) {
      state.all = stocks;
    },
    ADD_ADDITONAL_STOCKS(state, stocks) {
      stocks.forEach((stock) => {
        state.all.push(stock);
      });
    },
    UPDATE_CURRENT_PAGE(state) {
      state.pagination.currentPage = state.pagination.currentPage + 1;
    },
    RESET_CURRENT_PAGE(state) {
      state.pagination.currentPage = 0;
    },
    SET_TOTAL_ITEMS(state, totalItems) {
      state.pagination.totalItems = totalItems;
    },
    UPDATE_MARKET(state, country) {
      state.app.market = country.label;
    },
    UPDATE_COUNTRY_FILTER(state, country) {
      if (country.value === "global") {
        state.filters.countryFilter = [];
      } else {
        state.filters.countryFilter = [["country_name", "in", [country.value]]];
      }
    },
    UPDATE_SORT_FILTER(state, country) {
      if (country === "desc") {
        state.filters.sortByFilter = ["order_by", "market_cap", "desc"];
      } else {
        state.filters.sortByFilter = ["order_by", "market_cap", "asc"];
      }
    },
    TOGGLE_LOADING(state) {
      state.app.isLoading = !state.app.isLoading;
    },
    TOGGLE_PAGANATION_LOADING(state) {
      state.pagination.isLoading = !state.pagination.isLoading;
    },
    UPDATE_ERROR(state, msg) {
      state.app.hasError = msg;
    },
  },

  actions: {
    updateCountryFilter(ctx, country) {
      ctx.commit("UPDATE_COUNTRY_FILTER", JSON.parse(country));
      ctx.commit("UPDATE_MARKET", JSON.parse(country));
      ctx.dispatch("fetchStocks");
    },
    updateSortFilter(ctx, order) {
      ctx.commit("UPDATE_SORT_FILTER", order);
      ctx.dispatch("fetchStocks");
    },

    async fetchStocks(ctx) {
      ctx.commit("TOGGLE_LOADING");
      let filterRules = [
        ctx.state.filters.sortByFilter,
        ...ctx.state.filters.defaultFilters,
        ...ctx.state.filters.countryFilter,
      ];

      axios
        .post(
          "https://api.simplywall.st/api/grid/filter?include=info,score",
          {
            id: 0,
            no_result_if_limit: false,
            offset:
              ctx.state.pagination.itemsPerPage *
              ctx.state.pagination.currentPage,
            size: ctx.state.pagination.itemsPerPage,
            state: "read",
            rules: JSON.stringify(filterRules),
          },
          {
            headers: {
              "content-type": "application/json",
            },
          }
        )
        .then(function (response) {
          let responseData = response.data;
          ctx.commit("RESET_CURRENT_PAGE");
          ctx.commit("UPDATE_STOCKS", responseData.data);
          ctx.commit("SET_TOTAL_ITEMS", responseData.meta.total_records);
          ctx.commit("TOGGLE_LOADING");
          ctx.commit("UPDATE_ERROR", false);
        })
        .catch(function () {
          ctx.commit("TOGGLE_LOADING");
          ctx.commit("UPDATE_STOCKS", []);
          ctx.commit("UPDATE_ERROR", true);
        });
    },
    async loadMoreStocks(ctx) {
      ctx.commit("UPDATE_CURRENT_PAGE");
      ctx.commit("TOGGLE_PAGANATION_LOADING");

      let filterRules = [
        ctx.state.filters.sortByFilter,
        ...ctx.state.filters.defaultFilters,
        ...ctx.state.filters.countryFilter,
      ];

      axios
        .post(
          "https://api.simplywall.st/api/grid/filter?include=info,score",
          {
            id: 0,
            no_result_if_limit: false,
            offset:
              ctx.state.pagination.itemsPerPage *
              ctx.state.pagination.currentPage,
            size: ctx.state.pagination.itemsPerPage,
            state: "read",
            rules: JSON.stringify(filterRules),
          },
          {
            headers: {
              "content-type": "application/json",
            },
          }
        )
        .then(function (response) {
          let responseData = response.data;
          ctx.commit("ADD_ADDITONAL_STOCKS", responseData.data);
          ctx.commit("TOGGLE_PAGANATION_LOADING");
          // ctx.commit("SET_TOTAL_ITEMS", responseData.meta.total_records);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
};
