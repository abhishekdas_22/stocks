import { createStore } from "vuex";
import { stocks } from "./stocks.js";

export default createStore({
  modules: {
    stocks,
  },
});
